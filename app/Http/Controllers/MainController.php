<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceCategory;

class MainController extends Controller
{

    public function index(ServiceCategory $serviceCategory)
    {
        $serviceCategories = $serviceCategory::orderBy('service_category_name')
                                            ->get();
        return view('index', compact('serviceCategories'));
    }

}
