<nav class="uk-navbar-container" uk-navbar>
    <a class="uk-navbar-item uk-logo uk-width-1-6" href="{{ url('/') }}"><img src="{{ asset('images/Aurora-Logo-black.png') }}"></a>
    <div class="uk-navbar-right">
        <ul class="uk-navbar-nav">
            <li class="uk-active"><a href="#">Active</a></li>
            <li>
                <a href="#">Parent</a>
                <div class="uk-navbar-dropdown">
                    <ul class="uk-nav uk-navbar-dropdown-nav">
                        <li class="uk-active"><a href="#">Active</a></li>
                        <li><a href="#">Item</a></li>
                        <li><a href="#">Item</a></li>
                    </ul>
                </div>
            </li>
            <li><a href="#">Item</a></li>
        </ul>
    </div>
</nav>