<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts/partials/head')
</head>
<body>

    <header>
        @include('layouts/partials/header')       
    </header>
    
    <main class="py-4">
        @yield('content')
    </main>

    <footer>
        @include('layouts/partials/footer')
    </footer>

    <!-- footerScript -->
    @include('layouts/partials/foot')

    <!-- App js -->
    <script src="{{ asset('js/app.js')}}"></script>
</body>
</html>
