@extends('layouts.app')

@section('content')

<div class="uk-container">
    <h1 class="uk-text-lighter uk-padding-medium">Choose Your Glow up</h1>

    <h4 class="uk-text-light uk-padding-small">Select a category</h4>
    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1" uk-slider>
        <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@m uk-grid">
            @foreach($serviceCategories as $serviceCategory)
            @if($serviceCategory['is_active'] == TRUE)
                <li>
                    <div class="uk-panel">
                        <img src="{{$serviceCategory['service_category_image']}}" alt="">
                        <div class="uk-position-center uk-panel"><h1>{{$serviceCategory['service_category_name']}}</h1></div>
                    </div>
                </li>
            @endif
            @endforeach
        </ul>

        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>

    </div>

</div>

@endsection
